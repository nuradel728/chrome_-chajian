let importJs = document.createElement('script')
importJs.src = 'https://code.jquery.com/jquery-3.1.1.min.js';
importJs.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(importJs);
let bt = document.createElement('button');
bt.style.color = "red";
bt.innerText = "获取评论";
bt.setAttribute('id', 'hqplbt');
document.getElementById('cm_cr-buy_box').appendChild(bt);

function downloadJSON2CSV(objArray) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

    let str = '';

    for (let i = 0; i < array.length; i++) {
        let line = '';

        for (let index in array[i]) {
            line += array[i][index] + ',';
        }

        line.slice(0, line.length - 1);

        str += line + '\r';
    }
    window.open("data:text/csv;charset=utf-8," + str)
}

bt.onclick = function () {
    //获取评论主体
    let Plbodys = document.getElementsByClassName('a-section review aok-relative');
    let PLData = [];
    let TitleData = {
        time: undefined,
        star: undefined,
        user: undefined,
        textBody: undefined
    }
    //设置标题
    TitleData.user = '用户'
    TitleData.star = '评分'
    TitleData.time = '时间'
    TitleData.textBody = '评论内容'
    PLData.push(TitleData)
    for (let i = 0; i < Plbodys.length; i++) {
        //评论主体对象
        let PlDX = {
            time: undefined,
            star: undefined,
            user: undefined,
            textBody: undefined
        }
        let obj = Plbodys[i];
        //获取用户
        let userName = obj.getElementsByClassName('a-profile')[0].innerText
        //获取评分
        let star = obj.getElementsByClassName('a-link-normal')[0].getAttribute('title')
        //获取时间
        let time = obj.getElementsByClassName('a-size-base a-color-secondary review-date')[0].innerText
        //获取评论内容
        let textBody = obj.getElementsByClassName('a-row a-spacing-small review-data')[0].innerText
        //封装
        PlDX.user = userName
        PlDX.star = star
        PlDX.time = time
        PlDX.textBody = textBody
        PLData.push(PlDX)
    }

    downloadJSON2CSV(PLData)
}
